#-*- coding: iso-8859-1 -*-  #alfabeto latino
import cherrypy              # carregando biblioteca 

class HelloWorld:
	def index(self):
	    return "Hello World"
	index.exposed = True     # permite definir quais métodos serão expostos na web

cherrypy.quickstart(HelloWorld())    #publica instância e inicia o servidor